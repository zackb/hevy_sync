import json
from pathlib import Path
import datetime
import hevy_api
import sys

#login to be used in automation
hevy_api.login(sys.argv[1], sys.argv[2])

hevy_api.routines_sync_batch()
f = open("routines.json")
hevy_routines_data = json.load(f)
f = open("workout_program.json")
self_workout_data = json.load(f)


week_number = datetime.date.today().isocalendar()[1]


def update_week():
    if self_workout_data['updated_on_week'] == week_number:
        return
    else:
        if (self_workout_data['week'] == 3):
            self_workout_data['week'] = 1
        else:
            self_workout_data['week'] += 1
    self_workout_data['updated_on_week'] = week_number
    f = open("workout_program.json", "w")
    json.dump(self_workout_data, f)


def update_routines_json():
    for routine in hevy_routines_data['updated']:
        for exercise in routine['exercises']:
            if (exercise['title'] in self_workout_data['exercises']):
                i = self_workout_data['exercises'].index(exercise['title'])
                current_week = self_workout_data['week']
                weekly_percentages = self_workout_data['weekly_percentages'].get(
                    str(current_week))
                reps = self_workout_data['reps_per_week'].get(
                    str(current_week))
                for set in exercise['sets']:
                    current_set = set['index']
                    set['weight_kg'] = int(
                        (self_workout_data['maxes'][i]*0.95) * weekly_percentages[current_set])
                    set['reps'] = int(reps[current_set])
    f = open("routines.json", "w")
    json.dump(hevy_routines_data, f)


def send_all_routines_to_hevy():
    f = open("routines.json")
    routines = json.load(f)
    my_routine = {}

    for routine in routines["updated"]:
        my_routine["routine"] = ""
        my_routine["routine"] = routine
        hevy_api.update_routine(my_routine)


update_week()
update_routines_json()
send_all_routines_to_hevy()
routines_path = Path('routines.json')
routines_path.unlink()
