FROM python


RUN apt-get update && apt-get -y install cron vim

WORKDIR /app

COPY crontab /etc/cron.d/crontab
COPY hevy_api.py /app/hevy_api.py
COPY update_workouts.py /app/update_workouts.py
COPY workout_program.json /app/workout_program.json

RUN chmod 0644 /etc/cron.d/crontab
RUN /usr/bin/crontab /etc/cron.d/crontab

CMD ["cron", "-f"]
