
# Hevy_sync

A script to keep **Jim Wendler's 5/3/1** type workouts up-to-date in the **Hevy app** without the need to adjust every value of every set, of every workout, every week.

During a 5/3/1 workout program, you progress the *big lifts* (Squat, OHP, Deadlift, Bench Press) every week for 3 weeks, and then reset back to week 1. 

## Pre-deployment

### Run the setup.sh script and input your **Hevy** username, and passsword.

```bash
$./setup.sh
  Enter your username:
    thisisnotmyusername
  Enter your password:
    thisisnotmypassword
```

This will create a *crontab* with the appropriate values.
```bash
0 0 * * 0 python3 /app/update_workouts.py thisisnotmyusername thisisnotmypassword >>/tmp/out.log 2>/tmp/err.log
```

### Update the workout_program.js with your personal records and/or which week you're currently on.

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `week` | `int` | Which week you're currently doing in the program. If you just started set it to 1. The program advances from 1 to 3, with a deload week at 4. |
| `maxes`      | `[int]` | Update each value with your personal records. The order follows the same as the "exercises" column. |

Everything else is static, and should not be changed if you're a beginner.

## Deployment

To deploy this project run

```bash
  docker build -t hevy_sync .
  docker run hevy_sync
```