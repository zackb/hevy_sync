import getpass
import requests
import json
from pathlib import Path

HEADERS = {
    'x-api-key': 'shelobs_hevy_web',
    'Content-Type': 'application/json',
    'accept-encoding': 'gzip'
}


def login(user, password):
    s = requests.Session()
    r = s.post('https://api.hevyapp.com/login', data=json.dumps({'emailOrUsername': user, 'password': password}),
               headers=HEADERS)
    print(f"Status code (login): {r.status_code}")
    if r.status_code == 200:
        HEADERS["auth-token"] = r.json()['auth_token']


def routines_sync_batch(modified_routines=None):
    s = requests.Session()
    if modified_routines:
        r = s.post("https://api.hevyapp.com/routines_sync_batch",
                   data=json.dumps(modified_routines), headers=HEADERS)
    if modified_routines is None:
        r = s.post("https://api.hevyapp.com/routines_sync_batch",
                   headers=HEADERS)
    data = r.json()
    f = open("routines.json", "w")
    json.dump(data, f)
    print(f"Status code (routines_sync_batch): {r.status_code}")


def update_routine(modified_routine):
    s = requests.Session()
    r = s.put(f"https://api.hevyapp.com/routine/{modified_routine['routine']['id']}",
              data=json.dumps(modified_routine), headers=HEADERS)
    print(f"Status code (update_routine): {r.status_code}")

def routine_with_short_id(short_id):
    s = requests.Session()
    r = s.get(f"https://api.hevyapp.com/routine_with_short_id/{short_id}",
              headers=HEADERS)
    print(f"Status code (routine_with_short_id): {r.status_code}")
    print(r.json())